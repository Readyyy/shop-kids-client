import {Component, OnInit} from '@angular/core';
import {WsService} from '../../../services/api/ws.service';
import {Observable} from 'rxjs/Observable';
import {ResService} from '../../../services/api/res.service';
import Owl from '../../../plugins/owl';
declare const $: any;

@Component({
  selector   : 'app-index',
  templateUrl: './index.component.html',
  styleUrls  : ['./index.component.css']
})
export class IndexComponent implements OnInit {
  productsCart$: Observable<any>;
  test: Observable<{}>;
  slides = [];
  slidesService;

  productSales$: Observable<[any]>;
  $bestSales;
  $hotSales;

  imgService;

  productService;

  constructor(private wsService: WsService, private resService: ResService) {
    this.slidesService  = resService.service('slide');
    this.productService = wsService.service('products');
    this.imgService     = wsService.service('image');

    this.slidesService
      .find({
        query: {
          $sort : {updatedAt: -1},
          $limit: 5
        }
      }).then(res => {
      this.slides = res.data;
      this.setSlides();
    });

    this.productSales$ = this.productService
      .find({})
      .map(i => {
        this.setBestSales();
        this.setHotSales();
        return i.data;
      });
  }

  ngOnInit() {
  }

  setSlides() {
    setTimeout(() => {
      $('.tp-banner-v3').revolution(
        {
          delay         : 9000,
          startwidth    : 1175,
          startheight   : 590,
          hideThumbs    : 10,
          fullWidth     : 'on',
          forceFullWidth: 'on'
        });
    });
  }

  setBestSales() {
    setTimeout(() => {
      if (this.$bestSales) {
        this.$bestSales.reset();
      } else {
        this.$bestSales = new Owl('.pro_best_v3');
      }
    });
  }

  setHotSales() {
    setTimeout(() => {
      if (this.$hotSales) {
        this.$hotSales.reset();
      } else {
        this.$hotSales = new Owl('.product_best', {
          items             : 1,
          nav               : true,
          loop              : true,
          margin            : 10,
          autoplay          : false,
          autoplayTimeout   : 5000,
          autoplayHoverPause: true,
          responsive        : {
            0   : {
              items: 1,
            },
            480 : {
              items: 2,
            },
            767 : {
              items: 3,
            },
            1025: {
              items: 3,
            },
            1200: {
              items: 1,
            }
          }
        });
      }
    });
  }

}
