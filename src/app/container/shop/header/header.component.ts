import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '../../../services/auth.service';
import {WsService} from '../../../services/api/ws.service';
import {SharedService} from 'app/services/SharedService';
declare const $: any;

@Component({
  selector   : 'app-header',
  templateUrl: './header.component.html',
  styleUrls  : ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menu: any;
  menuIsOpen: Boolean = true;
  user;
  body: Element       = document.getElementsByTagName('body')[0];

  constructor(private translate: TranslateService, private wsService: WsService, private auth: AuthService, private sharedService: SharedService) {
    sharedService.on('reLogin', this.checkLogin.bind(this));
    this.checkLogin();
  }

  checkLogin() {
    this.auth.logIn().then(user => {
      this.user = user;
    }).catch(() => {
    });
  }

  toggleMenu() {
    this.menuIsOpen = !this.menuIsOpen;
    if (!this.menuIsOpen) {
      this.body.classList.add('fixed');
    } else {
      this.body.classList.remove('fixed');
    }
  }

  selectLang(lang: string) {
    this.translate.use(lang);
  }

  logout() {
    this.auth.logOut();
    delete this.user;
  }

  ngOnInit() {
  }

}
