import {Routes} from '@angular/router';
import {LayoutShopComponent} from './layout-shop/layout-shop.component';
import {IndexComponent} from './index/index.component';
import {LoginComponent} from './login/login.component';
export const shopRoutes: Routes = [
  {
    path     : '',
    component: IndexComponent
  },
  {
    path     : 'login',
    component: LoginComponent
  }
];
