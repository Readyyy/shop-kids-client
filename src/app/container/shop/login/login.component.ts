import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {ResService} from '../../../services/api/res.service';
import {Router} from '@angular/router';
import {SharedService} from '../../../services/SharedService';

@Component({
  selector   : 'app-login',
  templateUrl: './login.component.html',
  styleUrls  : ['./login.component.css']
})
export class LoginComponent {
  myForm: FormGroup;
  message: string;

  constructor(private auth: AuthService, private resService: ResService, private router: Router, private sharedService: SharedService) {
    this.myForm = new FormGroup({
      email   : new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
      password: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
    });
  }

  login() {
    if (this.myForm.valid) {
      this.auth.logIn({
        strategy: 'local',
        ...this.myForm.value
      }).then(() => {
        this.sharedService.broadcast('reLogin');
        this.router.navigate(['/']);
      }).catch(err => {
        this.message = 'Đăng nhập thất bại vui lòng thử lại';
      })
    }
  }

  Register() {
    if (this.myForm.valid) {
      this.resService.service('users')
        .create({
          fullName: 'admin',
          ...this.myForm.value
        })
        .then(() => {
          this.message = 'Đăng ký tài khoản thành công';
        }).catch(console.log)
    }
  }
}
