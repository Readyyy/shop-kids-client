import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutAdminComponent} from './layout-admin/layout-admin.component';
import {IndexAdminComponent} from './index-admin/index-admin.component';
import {ProductsComponent} from './products/products.component';
const routes: Routes = [
  {
    path     : '',
    component: LayoutAdminComponent,
    children : [
      {
        path     : '',
        component: ProductsComponent
      }
    ]
  }
];

@NgModule({
  imports  : [RouterModule.forChild(routes)],
  exports  : [RouterModule],
  providers: []
})
export class AdminRoutingModule {

}
