import {Component, OnInit} from '@angular/core';
declare const $: any;

@Component({
  selector   : 'app-layout-admin',
  templateUrl: './layout-admin.component.html'
})
export class LayoutAdminComponent implements OnInit {

  constructor() {
    $('link').remove();
    $('head').append('<link href="assets/css/admin.css" rel="stylesheet" type="text/css"/>')
  }

  ngOnInit() {
    $('.navbar-toggle').on('click', function () {
      $(this).toggleClass('open');
      $('#navigation').slideToggle(400);
    });
    $('.navigation-menu>li').slice(-1).addClass('last-elements');
    $('.navigation-menu li.has-submenu a[href="#"]').on('click', function (e) {
      e.preventDefault();
      if ($(window).width() < 992) {
        $('.navigation-menu ul.submenu,li').removeClass('open active');
        $(this).parent('li').addClass('open').find('.submenu:first').addClass('open');
      }
    });
    $('.navigation-menu li.has-submenu a[href!="#"]').on('click', function (e) {
      if ($(window).width() < 992) {
        $('.navbar-toggle').toggleClass('active open');
        $('#navigation').slideToggle(400);
      } else {
        $('.navigation-menu ul.submenu').removeClass('open active');
        $(this).parent().parent().addClass('open');
      }
      $('.navigation-menu li').removeClass('active open');
      $(this).parent().addClass('active').parent().parent().addClass('active');
    });
    $('.navigation-menu a').each(function () {
      if (this.href == window.location.href) {
        $(this).parent().addClass('active open'); // add active to li of the current link
        $(this).parent().parent().addClass('open');
        $(this).parent().parent().parent().addClass('active open'); // add active class to an anchor
      }
    });
  }

}
