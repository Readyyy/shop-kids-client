import {Component, OnInit} from '@angular/core';
import {WsService} from '../../../services/api/ws.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector   : 'app-products',
  templateUrl: './products.component.html',
  styleUrls  : ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Observable<any>;
  private productService: any;
  private imgService: any;
  private textSearch: string;

  constructor(private ws: WsService) {
    this.productService = ws.service('products');
    this.imgService     = ws.service('image');
    this.products       = Observable.combineLatest(
      this.productService.find({}),
      this.imgService.find({})
    ).map(([pros, img]) => {
      return pros['data'].map(pro => {
        pro.img = img['data'].filter(i => i.idProductId == pro.id);
        return pro;
      })
    })
  }

  ngOnInit() {
  }

  search() {
    console.log('sua');
    this.products = this.products.map(item => item.filter(i => i.name.indexOf(this.textSearch) != -1));
  }

}
