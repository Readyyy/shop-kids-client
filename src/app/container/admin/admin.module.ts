import {NgModule} from '@angular/core';
import {LayoutAdminComponent} from './layout-admin/layout-admin.component';
import {IndexAdminComponent} from './index-admin/index-admin.component';
import {AdminRoutingModule} from './admin-routing';
import {ProductsComponent} from './products/products.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';


@NgModule({
  imports     : [AdminRoutingModule, CommonModule, FormsModule],
  declarations: [LayoutAdminComponent, IndexAdminComponent, ProductsComponent]
})
export class AdminModule {
}
