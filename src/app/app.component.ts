import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.css']
})
export class AppComponent {
  constructor(translate: TranslateService) {
    translate.addLangs(['vi', 'en']);
    const langCurent = translate.getBrowserLang();
    if (translate.getLangs().indexOf(langCurent) === -1) {
      translate.use('en');
    } else {
      translate.use(langCurent);
    }
  }
}
