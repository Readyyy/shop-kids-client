import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
export class SharedService {
  observable: Observable<any>;
  observer: Observer<any>;

  constructor() {
    this.observable = Observable.create((observer: Observer<any>) => {
      this.observer = observer;
    }).share();
  }

  broadcast(event) {
    this.observer.next(event);
  }

  on(eventName, callback) {
    this.observable.filter(event => event === eventName).subscribe(callback);
  }
}
