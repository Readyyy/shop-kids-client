import {Injectable} from '@angular/core';
import * as feathers from 'feathers/client';
import * as hooks from 'feathers-hooks';
import * as rest from 'feathers-rest/client';
import * as authentication from 'feathers-authentication-client';
import axios from 'axios';

@Injectable()
export class ResService {

  private _feathers: any;

  constructor() {
    this._feathers = feathers()
      .configure(rest('http://localhost:3003')['axios'](axios))
      .configure(hooks())
      .configure(authentication({
        storage: window.localStorage
      }));
  }

  // expose services
  public service(name: string) {
    return this._feathers.service(name);
  }

  // expose authentication
  public authenticate(credentials?): Promise<any> {
    return this._feathers.authenticate(credentials);
  }

  public verifyJWT({accessToken}) {
    return this._feathers.passport.verifyJWT(accessToken);
  }

  // expose logout
  public logout() {
    return this._feathers.logout();
  }

}
