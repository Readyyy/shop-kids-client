import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {WsService} from './api/ws.service';


@Injectable()
export class AuthService {
  private serviceUsers;

  constructor(private router: Router, private resService: WsService) {
    this.serviceUsers = this.resService.service('users');
  }

  public logIn(credentials?): Promise<any> {
    return this.resService
      .authenticate(credentials)
      .then(token => this.resService.verifyJWT(token))
      .then(user => this.saveProfile(user))
      .catch(err => {
        localStorage.removeItem('profile');
        return Promise.reject(err);
      });
  }

  public isLogin(): Promise<any> {
    return this.resService.authenticate()
      .then(token => this.resService.verifyJWT(token))
      .then(user => {
        if (!JSON.parse(localStorage.getItem('profile'))) {
          return this.saveProfile(user);
        } else {
          return JSON.parse(localStorage.getItem('profile'));
        }
      })
      .catch(err => {
        localStorage.removeItem('profile');
        return Promise.reject(err);
      });
  }

  private saveProfile({userId}): Promise<boolean> {
    return this.serviceUsers.get(userId + '')
      .then(user => {
        localStorage.setItem('profile', JSON.stringify(user));
        return user;
      });
  }

  public logOut() {
    localStorage.removeItem('profile');
    this.resService.logout();
  };

}
