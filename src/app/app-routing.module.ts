import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// import {AuthGuard} from './guards/auth.guard';
import {shopRoutes} from './container/shop/shop-routing';
import {LayoutShopComponent} from './container/shop/layout-shop/layout-shop.component';

const routes: Routes = [
  {
    path     : '',
    component: LayoutShopComponent,
    children : [...shopRoutes]
  },
  {
    path        : 'admin',
    loadChildren: 'app/container/admin/admin.module#AdminModule'
  },
  {path: '**', redirectTo: '/admin', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
