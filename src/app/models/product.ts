export default class Product {
  id: number;
  text: string;
  read: boolean;
  createdAt: any;
  updatedAt: any;
}
