import {Directive, Input} from '@angular/core';
import {NG_VALIDATORS, Validator, AbstractControl} from '@angular/forms';
import {Validator as vali} from 'validator.ts/Validator';

@Directive({
  selector: '[number]',
  providers: [{provide: NG_VALIDATORS, useExisting: NumberValidator, multi: true}]
})
export class NumberValidator implements Validator {

  @Input('number') type: string;
  @Input('MinLength') min: number;
  @Input('MaxLength') max: number;

  validate(c: AbstractControl): { [p: string]: any } {
    let {min, max} = this;
    if (this.type == 'float') {
      if (!vali.prototype.isFloat(c.value, {})) return {'number': 'number not validate'};
      else if (min >= parseFloat(c.value) || max <= parseFloat(c.value)) return {'numberLength': 'Non-standard numbers'};
    }
    if (this.type == 'int') {
      if (!vali.prototype.isInt(c.value, {})) return {'number': 'number not validate'};
      else if ((min && min >= parseInt(c.value)) || (max && max <= parseInt(c.value))) return {'numberLength': 'Non-standard numbers'};
    }
    return undefined;
  }

}
