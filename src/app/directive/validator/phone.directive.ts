import {Directive} from '@angular/core';
import {NG_VALIDATORS, Validator, AbstractControl} from "@angular/forms";
import {Validator as vali} from "validator.ts/Validator";

@Directive({
  selector: '[phone]',
  providers: [{provide: NG_VALIDATORS, useExisting: PhoneValidator, multi: true}]
})
export class PhoneValidator implements Validator {

  validate(c: AbstractControl): {[p: string]: any} {
    if (!c.value || !vali.prototype.isMobilePhone(c.value, 'vi-VN')) {
      return {phone: "phone not validate"}
    }
    return undefined;
  }

}
