import {Directive} from '@angular/core';
import {Validator, AbstractControl, NG_VALIDATORS} from '@angular/forms';
import {Validator as vali} from 'validator.ts/Validator';

@Directive({
  selector : '[email]',
  providers: [{provide: NG_VALIDATORS, useExisting: EmailValidator, multi: true}]
})
export class EmailValidator implements Validator {

  validate(c: AbstractControl): { [p: string]: any } {
    if (!vali.prototype.isEmail(c.value, {})) {
      return {email: 'Email not validate'}
    }
    return undefined;
  }

}
