import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value && value.toString().replace(/./g, (c, i, a) => i && c !== '.' && ((a.length - i) % 3 === 0) ? ',' + c : c) + 'đ';
  }
}
