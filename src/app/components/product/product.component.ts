import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector   : 'app-product',
  templateUrl: './product.component.html',
  styleUrls  : ['./product.component.css']
})
export class ProductComponent implements OnChanges {
  @Input() product: any;
  @Input() img: any;
  @Input() type: string;
  @Input() dateSale: any;

  hours: number;
  mins: number;
  secs: number;

  constructor() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.dateSale) {
      const end   = moment(this.dateSale, 'DD/MM/YYYY');
      const count = setInterval(() => {
        const duration = moment.duration(moment(new Date()).diff(end));
        if (duration.asSeconds() > 0) {
          clearInterval(count);
          delete this.dateSale;
        }
        this.hours = Math.ceil(duration.asHours()) * -1;
        this.mins  = Math.ceil(duration.asMinutes() % 60) * -1;
        this.secs  = Math.ceil(duration.asSeconds() % 60) * -1;
      }, 1000);
    }
  }

}
