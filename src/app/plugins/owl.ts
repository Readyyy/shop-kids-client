declare const $: any;

export default class Owl {
  private owl: any;
  private options;

  constructor(dom, options: any = {
    nav               : true,
    loop              : true,
    margin            : 30,
    autoplay          : false,
    autoplayTimeout   : 5000,
    autoplayHoverPause: true,
    responsiveClass   : true,
    responsive        : {
      0   : {
        items: 1,
      },
      480 : {
        items: 2,
      },
      767 : {
        items: 3,
      },
      1025: {
        items: 4,
      }
    }
  }) {
    this.owl     = $(dom);
    this.options = options;
    this.owl.owlCarousel(options);
  }

  trigger(...args) {
    return this.owl.trigger.apply(this, args);
  }

  destroy() {
    this.owl.owlCarousel('destroy');
  }

  reset() {
    this.owl.owlCarousel('destroy');
    this.owl.owlCarousel(this.options);
    const index = this.owl.find('.owl-item').index(this.owl.find('.owl-item:empty'));
    if (index !== -1) {
      this.owl.trigger('remove.owl.carousel', index);
      this.owl.trigger('refresh.owl.carousel');
    }
  }

  autoplay() {
    this.owl.trigger('play.owl.autoplay', [1000]);
  }

  stop() {
    this.owl.trigger('stop.owl.autoplay');
  }
}
