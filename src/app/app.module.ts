import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Http, HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {AuthService} from './services/auth.service';
import {AppRoutingModule} from './app-routing.module';
import {AuthGuard} from './guards/auth.guard';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ResService} from './services/api/res.service';
import {WsService} from './services/api/ws.service';
import {IndexComponent} from './container/shop/index/index.component';
import {HeaderComponent} from './container/shop/header/header.component';
import {FooterComponent} from './container/shop/footer/footer.component';
import {LayoutShopComponent} from './container/shop/layout-shop/layout-shop.component';
import {ProductComponent} from './components/product/product.component';
import {LoginComponent} from './container/shop/login/login.component';
import {SharedService} from './services/SharedService';

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    // shop
    HeaderComponent,
    FooterComponent,
    IndexComponent,
    LayoutShopComponent,
    ProductComponent,
    LoginComponent,
  ],
  imports     : [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide   : TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps      : [Http]
      }
    })
  ],
  providers   : [
    WsService,
    ResService,
    AuthService,
    AuthGuard,
    SharedService
  ],
  bootstrap   : [
    AppComponent
  ]
})
export class AppModule {
}
